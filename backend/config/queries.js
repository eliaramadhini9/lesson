const Pool = require('pg').Pool
const pool = new Pool({
  user: 'elia',
  host: 'localhost',
  database: 'currency',
  password: 'password',
  port: 5432,
})

const getAll = (request, response) => {
  pool.query('SELECT * FROM currency_data ORDER BY country ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getOne = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM currency_data WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createData = (request, response) => {
  const { country, value } = request.body

  pool.query('INSERT INTO currency_data (country, value) VALUES ($1, $2)', [country, value], (error, results) => {

    response.status(201).json(`Success!`)
  })
}

const updateData = (request, response) => {
  const id = parseInt(request.params.id)
  const { country, value } = request.body

  pool.query(
    'UPDATE currency_data SET country = $1, value = $2 WHERE id = $3',
    [country, value, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Success!`)
    }
  )
}

const deleteData = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM currency_data WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(`Success!`)
  })
}

const getUsers = (request, response) => {
  pool.query('SELECT * FROM users', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getUser = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
      console.log(error);
    }

    response.status(200).json(results.rows)
  })
}

const getUserByID = (request, response) => {
  const user_id = parseInt(request.params.user_id)

  pool.query('SELECT * FROM users WHERE user_id = $1', [user_id], (error, results) => {
    if (error) {
      throw error
    }

    response.status(200).json(results.rows[0])
  })
}

const createUser = (request, response) => {
  const { user_id, name, position, country } = request.body

  pool.query('INSERT INTO users (user_id, name, position, country) VALUES ($1, $2, $3, $4)', [user_id, name, position, country], (error, results) => {
    console.log(results.rows);
    console.log(results.rows[0]);
    response.status(201).json(results.rows)
  })
}

const getNotes = (request, response) => {
  pool.query('SELECT * FROM notes', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getNotesByUser = (request, response) => {
  const user_id = request.params.user_id
  pool.query('SELECT * FROM notes WHERE user_id = $1 ORDER BY priority DESC' , [user_id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getNote = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM notes WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createNote = (request, response) => {
  const { title, note, date, user_id, priority } = request.body

  pool.query('INSERT INTO notes (title, note, date, user_id, priority) VALUES ($1, $2, $3, $4, $5)', [title, note, date, user_id, priority], (error, results) => {

    response.status(201).json(`Success!`)
  })
}

const updateNote = (request, response) => {
  const id = parseInt(request.params.id)
  const { title, note, priority } = request.body

  pool.query(
    'UPDATE notes SET title=$1, note=$2, priority=$3 WHERE id = $4',
    [title, note, priority, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Success!`)
    }
  )
}

const deleteNote = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM notes WHERE id = $1', [id], (error, results) => {
    console.log(results);
    if (error) {
      throw error
    }
    response.status(200).json(`Success!`)
  })
}


const getSites = (request, response) => {
  pool.query('SELECT * FROM sites', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getPlants = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * from plants join sites on (plants.site_id = sites.site_id) where sites.site_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getDepartements = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * from departements join plants on (departements.plant_id = plants.plant_id) where plants.plant_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getWorkCenters = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * from work_centers join departements on (work_centers.departement_id = departements.departement_id) where departements.departement_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getWorkStations = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * from work_stations join work_centers on (work_stations.work_center_id = work_centers.work_center_id) where work_centers.work_center_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getAssets = (request, response) => {
  const id = parseInt(request.params.id)
  const limit = parseInt(request.params.limit)
  const offset = parseInt(request.params.offset)


  pool.query('SELECT * from assets join work_stations on (assets.workstation_id = work_stations.workstation_id) where work_stations.workstation_id = $1 LIMIT $2 OFFSET $3', [id, limit, offset], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getAllAssets = (request, response) => {
  const id = parseInt(request.params.id)


  pool.query('SELECT * from assets join work_stations on (assets.workstation_id = work_stations.workstation_id) where work_stations.workstation_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getTransactions = (request, response) => {
  const id = parseInt(request.params.id)
  const limit = parseInt(request.params.limit)
  const offset = parseInt(request.params.offset)

  pool.query('SELECT assets.*, transactions.* from assets join work_stations on (assets.workstation_id=work_stations.workstation_id) join transactions on (assets.asset_id=transactions.asset_id) where work_stations.workstation_id = $1 order by timestamp asc LIMIT $2 OFFSET $3', [id, limit, offset], (error, results) => {
    if (error) {
      throw error
    }

    console.log(results.rows)
    response.status(200).json(results.rows)
  })
}

const getTimes = (request, response) => {
  const asset_id = parseInt(request.params.asset_id)
  const date = request.params.date
  const start = date + ' 00:00:01'
  const end = date + ' 23:59:59'

  pool.query('with prev_data as ((select timestamp as start_time, value from transactions t where DATE(timestamp) < DATE($2) and asset_id = $1 order by timestamp desc limit 1) union all (select timestamp as start_time, value from transactions t where DATE(timestamp) = DATE($2) and asset_id = $1) union all (select timestamp as start_time, value from transactions t where DATE(timestamp) > DATE($2) and asset_id = $1 order by timestamp asc limit 1)), next_data as (select case when ((row_number() OVER (ORDER BY start_time asc) = 1) and (DATE(start_time) < DATE($2))) then to_timestamp($3, \'YYYY-MM-DD HH24:MI:SS\') else start_time end as start_time, case when ((row_number() OVER (ORDER BY start_time desc) = 1) and (DATE(start_time) > DATE($2))) then to_timestamp($4, \'YYYY-MM-DD HH24:MI:SS\') else lead(start_time,1) OVER ( ORDER BY start_time) end as end_time, value from prev_data) select start_time, case when ((row_number() OVER (ORDER BY end_time desc) = 1) and (DATE(end_time) > (DATE(start_time)))) then to_timestamp($4, \'YYYY-MM-DD HH24:MI:SS\') when ((row_number() OVER (ORDER BY end_time desc) = 1) and (end_time is NULL)) then start_time else end_time end as end_time, value from next_data where DATE(start_time) = DATE($2) order by start_time', [asset_id, date, start, end], (error, results) => {
    if (error) {
      throw error
    }

    console.log(results.rows)
    response.status(200).json(results.rows)
  })
}




module.exports = {
  getAll,
  getOne,
  createData,
  updateData,
  deleteData,
  getUsers,
  getUser,
  getUserByID,
  createUser,
  getNotes,
  getNote,
  getNotesByUser,
  createNote,
  updateNote,
  deleteNote,

  getSites,
  getPlants,
  getDepartements,
  getWorkCenters,
  getWorkStations,
  getAssets,
  getAllAssets,
  getTransactions,
  getTimes
}

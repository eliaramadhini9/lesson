--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Ubuntu 13.2-1.pgdg18.04+1)
-- Dumped by pg_dump version 13.2 (Ubuntu 13.2-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: currency_data; Type: TABLE; Schema: public; Owner: elia
--

CREATE TABLE public.currency_data (
    id integer NOT NULL,
    country character varying(30),
    value numeric(6,0)
);


ALTER TABLE public.currency_data OWNER TO elia;

--
-- Name: currency_data_id_seq; Type: SEQUENCE; Schema: public; Owner: elia
--

CREATE SEQUENCE public.currency_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.currency_data_id_seq OWNER TO elia;

--
-- Name: currency_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: elia
--

ALTER SEQUENCE public.currency_data_id_seq OWNED BY public.currency_data.id;


--
-- Name: currency_data id; Type: DEFAULT; Schema: public; Owner: elia
--

ALTER TABLE ONLY public.currency_data ALTER COLUMN id SET DEFAULT nextval('public.currency_data_id_seq'::regclass);


--
-- Data for Name: currency_data; Type: TABLE DATA; Schema: public; Owner: elia
--

COPY public.currency_data (id, country, value) FROM stdin;
57	Vietnam	444
59	indonesia	12345
60	indonesia	12345
61	indonesia	12345
65	percobaan	11111
\.


--
-- Name: currency_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: elia
--

SELECT pg_catalog.setval('public.currency_data_id_seq', 67, true);


--
-- Name: currency_data currency_data_pkey; Type: CONSTRAINT; Schema: public; Owner: elia
--

ALTER TABLE ONLY public.currency_data
    ADD CONSTRAINT currency_data_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--


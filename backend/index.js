

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const cors= require('cors')
const db = require('./config/queries')
const port = 3001

app.use(cors())

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.use(express.static("repository"))

app.get('/currency', db.getAll)
app.get('/currency/:id', db.getOne)
app.post('/currency', db.createData)
app.put('/currency/:id', db.updateData)
app.delete('/currency/:id', db.deleteData)

app.get('/users', db.getUsers)
app.get('/users/:id', db.getUser)
app.get('/user/:user_id', db.getUserByID)
app.post('/user', db.createUser)

app.get('/notes', db.getNotes)
app.get('/notes/:id', db.getNote)
app.get('/note/:user_id', db.getNotesByUser)
app.post('/addnote', db.createNote)
app.put('/updatenote/:id', db.updateNote)
app.delete('/deletenote/:id', db.deleteNote)

app.get('/sites', db.getSites)
app.get('/plants/:id', db.getPlants)
app.get('/departements/:id', db.getDepartements)
app.get('/work_centers/:id', db.getWorkCenters)
app.get('/work_stations/:id', db.getWorkStations)
app.get('/assets/:id/:limit/:offset', db.getAssets)
app.get('/allassets/:id', db.getAllAssets)
app.get('/transactions/:id/:limit/:offset', db.getTransactions)
app.get('/times/:asset_id/:date', db.getTimes)

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})

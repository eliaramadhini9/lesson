import React, { useState} from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios'
import './login.css';

function Login() {
  const history = useHistory();
  const navigateTo = () => history.push('/')
  const initialState = {
    userData: null,
    userLoged: [],
    user_id: '',
    name: '',
    position: '',
    country: '',
    isLoged: false,
    isRegistered: true
  }

  const [state, setState] = useState(initialState)
  // 
  // const getUsers = () => {
  //   let config = {
  //     method: 'get',
  //     url: 'http://localhost:3001/users'
  //   }
  //   axios(config)
  //     .then((res) => {
  //       setState({
  //         ...state,
  //         userData: res.data
  //       })
  //     })
  //     .catch(err => {
  //       console.log(err)
  //     })
  // }

  const login = (e) => {
    e.preventDefault();
    let config = {
      method: 'get',
      url: 'http://localhost:3001/user/' + state.user_id
    }
    axios(config)
      .then((res) => {
        if (res.data.user_id !== undefined) {
          setState({
            ...state,
            isLoged: true
          })
          localStorage.setItem('isLoged', true)
          localStorage.setItem('user_id', res.data.user_id)
          localStorage.setItem('name', res.data.name)
          localStorage.setItem('position', res.data.position)
          localStorage.setItem('country', res.data.country)

          navigateTo()

        } else {
          setState({
            ...state,
            isRegistered: false
          })
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const register = (e) => {
    e.preventDefault();

    let config = {
      method: 'post',
      url: 'http://localhost:3001/user',
      data: {
        user_id: state.user_id,
        name: state.name,
        position: state.position,
        country: state.country
      }
    }
    axios(config)
      .then((res) => {
        setState({
          ...state,
          isLoged: true,
          isRegistered: true
        })
        localStorage.setItem('isLoged', true)
        localStorage.setItem('user_id', state.user_id)
        localStorage.setItem('name', state.name)
        localStorage.setItem('position', state.position)
        localStorage.setItem('country', state.country)

        navigateTo()

      })
      .catch(err => {
        console.log(err)
      })
  }

  const handleChange = (e) => {
    const {
      name,
      value
    } = e.target
    setState({
      ...state,
      [name]: value
    });
  }

  return (
    <div className='App'>
      <div className="login">
        <div className="header">Login</div>
        <div className="form">
        {state.isRegistered ?
          <form id='loginInput' onSubmit={login}>
            <label> ID </label>
            <input type="text" name="user_id" value={state.user_id} onChange={handleChange} />
          </form>
          :
          <form id='loginInput' onSubmit={register}>
            <label> ID </label>
            <input type="text" name="user_id" value={state.user_id} onChange={handleChange} required />
              <label> Name </label>
            <input type="text" name="name" value={state.name} onChange={handleChange} required />
              <label> Position </label>
            <input type="text" name="position" value={state.position} onChange={handleChange} required />
              <label> Country </label>
            <input type="text" name="country" value={state.country} onChange={handleChange} required />
          </form>
        }
        </div>
        <button className="button" form='loginInput'>
          Confirm
        </button>
        <button className="button" onClick={()=> {setState((prev) => ({
          ...prev, isRegistered: true
        }))}}>
          Cancel
        </button>
      </div>
    </div>
  )
}

export default Login;

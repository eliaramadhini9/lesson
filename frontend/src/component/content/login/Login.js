import Header from '../../Header.js'
import LoginPage from './loginPage.js'
import './login.css';

function Login() {
  return (
    <div className='container'>
      <Header />
      <LoginPage />
    </div>
  )
}

export default Login;

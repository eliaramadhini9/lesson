const getSites = () => {
    let config = {
      method: "get",
      url: `http://localhost:3001/sites`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        sites: res.data
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const getPlants = (id) => {
    let config = {
      method: "get",
      url: `http://localhost:3001/plants/${id}`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        plants: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const getDepartements = (id) => {
    let config = {
      method: "get",
      url: `http://localhost:3001/departements/${id}`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        departements: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const getWorkCenters = (id) => {
    let config = {
      method: "get",
      url: `http://localhost:3001/work_centers/${id}`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        work_centers: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const getWorkStations = (id) => {
    let config = {
      method: "get",
      url: `http://localhost:3001/work_Stations/${id}`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        work_stations: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const getAssets = (id) => {
    let config = {
      method: "get",
      url: `http://localhost:3001/assets/${id}`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        assets: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const getTransactions = (id) => {
    let config = {
      method: "get",
      url: `http://localhost:3001/transactions/${id}`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        transactions: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  export {getSites, getPlants, getDepartements}
import React, { useEffect, useState } from "react";
import { FaFilter, FaFileExport } from "react-icons/fa";
import {
    IoPlaySkipBackSharp,
    IoPlaySkipForwardSharp,
    IoCaretForwardSharp,
    IoCaretBackSharp,
} from "react-icons/io5";
import axios from "axios";
import "./machine.css";
import "../../../css/style.css";
import Logo from "../../../image/cadit.jpg";
import {MachineStatus, Utilization} from "./machineStatus.js";
import ReactExport from "react-export-excel";

function Machine() {
    const today = new Date(new Date().getTime() - (new Date().getTimezoneOffset() * 60000)).toISOString().slice(0,10)
    const limit = 4;

    const ExcelFile = ReactExport.ExcelFile;
    const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
    const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;



    const initialState = {
        sites: [],
        plants: [],
        departements: [],
        work_centers: [],
        work_stations: [],
        data: [],
        allData: [],
        transactions: [],
        site_id: 1,
        plant_id: 1,
        departement_id: 1,
        work_center_id: 1,
        workstation_id: 1,
        asset_id: 1,
        dateSelected: today,

        isShowing: true,
        currentPage: 1,
        totalPage: ''
    };

    const [state, setState] = useState(initialState);

    useEffect(() => {
        getSites();
        getPlants(state.site_id);
        getDepartements(state.plant_id);
        getWorkCenters(state.departement_id);
        getWorkStations(state.work_center_id);
        getData(state.workstation_id, state.dateSelected);

    }, [
        state.site_id,
        state.plant_id,
        state.departement_id,
        state.work_center_id,
        state.workstation_id,
        state.currentPage,
        state.dateSelected
    ]);

    const getSites = () => {
        let config = {
            method: "get",
            url: `http://localhost:3001/sites`,
        };
        axios(config)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    sites: res.data,
                }));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getPlants = (id) => {
        let config = {
            method: "get",
            url: `http://localhost:3001/plants/${id}`,
        };
        axios(config)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    plants: res.data,
                }));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getDepartements = (id) => {
        let config = {
            method: "get",
            url: `http://localhost:3001/departements/${id}`,
        };
        axios(config)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    departements: res.data,
                }));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getWorkCenters = (id) => {
        let config = {
            method: "get",
            url: `http://localhost:3001/work_centers/${id}`,
        };
        axios(config)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    work_centers: res.data,
                }));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getWorkStations = (id) => {
        let config = {
            method: "get",
            url: `http://localhost:3001/work_Stations/${id}`,
        };
        axios(config)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    work_stations: res.data,
                }));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getData = (id, date) => {
        let config1 = {
            method: "get",
            url: `http://localhost:3001/assets/${id}/${limit}/${(state.currentPage - 1) * limit}`,
        };
        axios(config1)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    data: res.data,
                }));
            })
            .catch((err) => {
                console.log(err);
            });


        let config2 = {
            method: "get",
            url: `http://localhost:3001/allassets/${id}`,
        };
        axios(config2)
            .then((res) => {
                const total = Math.ceil(res.data.length / 4)
                setState((prev) => ({
                    ...prev,
                    totalPage: total,
                }));
            })
            .catch((err) => {
                console.log(err);
            });


            let config3 = {
                method: "get",
                url: `http://localhost:3001/alltransactions/${id}/${date}`,
            };
            axios(config3)
                .then((res) => {
                    setState((prev) => ({
                        ...prev,
                        allData: res.data
                    }));
                })
                .catch((err) => {
                    console.log(err);
                });
    };

    const handleChange = (e) => {
        const { name, value } = e.target;

        setState((prev) => ({
            ...prev,
            [name]: value,
        }));
    };

    return (
        <div className='App'>
            <div className='machine'>
                {state.isShowing ? (
                    <div className='header'>
                        <form onSubmit={getData}>
                            <FaFilter
                                className='icon'
                                size={20}
                                style={{ color: "#aeb3b5" }}
                            />
                            <label>
                                Site:
                                <select
                                    className='classic'
                                    name='site_id'
                                    value={state.site_id}
                                    onChange={handleChange}>
                                    {state.sites.map((site, i) => (
                                        <option value={site.site_id}>
                                            {site.site_name}
                                        </option>
                                    ))}
                                </select>
                            </label>
                            <label>
                                Plant:
                                <select
                                    className='classic'
                                    name='plant_id'
                                    value={state.plant_id}
                                    onChange={handleChange}>
                                    {state.plants.map((plant, i) => (
                                        <option value={plant.plant_id}>
                                            {plant.plant_name}
                                        </option>
                                    ))}
                                </select>
                            </label>
                            <label>
                                Department:
                                <select
                                    className='classic'
                                    name='department_id'
                                    value={state.departement_id}
                                    onChange={handleChange}>
                                    {state.departements.map(
                                        (departement, i) => (
                                            <option
                                                value={
                                                    departement.departement_id
                                                }>
                                                {departement.departement_name}
                                            </option>
                                        )
                                    )}
                                </select>
                            </label>
                            <label>
                                Work Center:
                                <select
                                    className='classic'
                                    name='work_center_id'
                                    value={state.work_center_id}
                                    onChange={handleChange}>
                                    {state.work_centers.map((wc, i) => (
                                        <option value={wc.work_center_id}>
                                            {wc.work_center_name}
                                        </option>
                                    ))}
                                </select>
                            </label>
                            <label>
                                Workstation:
                                <select
                                    className='classic'
                                    name='workstation_id'
                                    value={state.workstation_id}
                                    onChange={handleChange}>
                                    {state.work_stations.map((ws, i) => (
                                        <option value={ws.workstation_id}>
                                            {ws.workstation_name}
                                        </option>
                                    ))}
                                </select>
                            </label>
                            <button className='search-button' type='submit'>
                                {" "}
                                Search{" "}
                            </button>
                            <ExcelFile element={<FaFileExport
                                size={20}
                                style={{ color: "#22e38c", cursor: "pointer" }} />}
                            >
                                <ExcelSheet data={state.allData} name="Transactions">
                                    <ExcelColumn label="Timestamp" value="timestamp" />
                                    <ExcelColumn label="Value" value="value" />
                                    <ExcelColumn label="State" value={(col) => col.value === 1 ? "Running" : col.value === 2 ? "Idle" : col.value === 3 ? "Down" : "Offline"} />
                                </ExcelSheet>
                            </ExcelFile>
                            <input type="date" id="calendar" name="dateSelected" value={state.dateSelected} onChange={handleChange} />
                        </form>
                    </div>
                ) : (
                    ""
                )}
                <div className='content'>
                    {state.isShowing ? (
                        <a
                            className='close'
                            onClick={() => {
                                setState((prev) => ({
                                    ...prev,
                                    isShowing: false,
                                }));
                            }}>
                            &times;
                        </a>
                    ) : (
                        <a className='filter'>
                            <FaFilter
                                className='icon'
                                size={15}
                                style={{ color: "white", cursor: "pointer" }}
                                onClick={() => {
                                    setState((prev) => ({
                                        ...prev,
                                        isShowing: true,
                                    }));
                                }}
                            />
                        </a>
                    )}
                    <div className='content-data'>
                        {state.data.map((data, i) => (
                            <div className='data'>
                                <div className='left'>
                                    <div className='name'>
                                        {data.asset_name}
                                    </div>
                                    <div className='image'>
                                        <img
                                            src={
                                                "http://localhost:3001/cadit.jpg"
                                            }
                                            alt='logo'
                                        />
                                    </div>
                                </div>
                                <div className='right'>
                                    <div className='utilization'>
                                        Utilization
                                        <Utilization
                                            asset_id={data.asset_id}
                                            date={state.dateSelected}
                                        />%
                                    </div>
                                    <div className='graphic'>
                                        <MachineStatus
                                            asset_id={data.asset_id}
                                            date={state.dateSelected}
                                        />
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                    <div className='footer'>
                        <div className='info'>
                            <div className='indicator'>
                                <svg width='20' height='20'>
                                    <circle
                                        cx='10'
                                        cy='10'
                                        r='6'
                                        fill='green'
                                    />
                                </svg>
                                <label style={{ color: "green" }}>
                                    Running
                                </label>
                            </div>
                            <div className='indicator'>
                                <svg width='20' height='20'>
                                    <circle
                                        cx='10'
                                        cy='10'
                                        r='6'
                                        fill='orange'
                                    />
                                </svg>
                                <label style={{ color: "orange" }}>Idle</label>
                            </div>
                            <div className='indicator'>
                                <svg width='20' height='20'>
                                    <circle cx='10' cy='10' r='6' fill='red' />
                                </svg>
                                <label style={{ color: "red" }}>Down</label>
                            </div>
                            <div className='indicator'>
                                <svg width='20' height='20'>
                                    <circle
                                        cx='10'
                                        cy='10'
                                        r='6'
                                        fill='black'
                                    />
                                </svg>
                                <label style={{ color: "black" }}>
                                    Offline
                                </label>
                            </div>
                        </div>
                        <div className='pagination'>
                            <span>
                                <IoPlaySkipBackSharp
                                    style={{
                                        color: "#045075",
                                        cursor: "pointer",
                                    }}
                                    onClick={() => {
                                        setState((prev) => ({
                                            ...prev,
                                            currentPage: 1,
                                        }));
                                    }}
                                />
                            </span>
                            <span>
                                <IoCaretBackSharp
                                    style={{
                                        color: "#045075",
                                        cursor: "pointer",
                                    }}
                                    onClick={() => {
                                        setState((prev) => ({
                                            ...prev,
                                            currentPage: state.currentPage !== 1 ? state.currentPage - 1 : state.currentPage,
                                        }));
                                    }}
                                />
                            </span>
                            <span>
                                <input
                                    type='text'
                                    value={state.currentPage + '/' + state.totalPage}
                                    disabled='disabled'
                                />
                            </span>
                            <span>
                                <IoCaretForwardSharp
                                    style={{
                                        color: "#045075",
                                        cursor: "pointer",
                                    }}
                                    onClick={() => {
                                        setState((prev) => ({
                                            ...prev,
                                            currentPage: state.currentPage !== state.totalPage ? state.currentPage + 1 : state.currentPage,
                                        }));
                                    }}
                                />
                            </span>
                            <span>
                                <IoPlaySkipForwardSharp
                                    style={{
                                        color: "#045075",
                                        cursor: "pointer",
                                    }}
                                    onClick={() => {
                                        setState((prev) => ({
                                            ...prev,
                                            currentPage: state.totalPage,
                                        }));
                                    }}
                                />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Machine;

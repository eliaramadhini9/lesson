import React, { useEffect, useState } from "react";
import { FaFilter, FaFileExport } from 'react-icons/fa'
import { IoPlaySkipBackSharp, IoPlaySkipForwardSharp, IoCaretForwardSharp, IoCaretBackSharp } from "react-icons/io5";
import { TimeSeries } from "pondjs";
import ReactApexChart from "react-apexcharts";
import axios from "axios";
import "./machine.css";
import "../../../css/style.css";
import Logo from '../../../image/cadit.jpg';
import Header from '../../Header.js'
import data from "./component/data";

function Machine() {
  const date = new Date()
  const today = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate()

  const initialState = {
      sites: [],
      site_id:1,
      plants:[],
      plant_id: '',
      isShowing: true,
      page: 1,
      maxPage: 5,
      series: [
        // Thomas Jefferson
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1790, 2, 22).getTime(),
                new Date(1793, 11, 31).getTime()
              ]
            }
          ]
        },
        // John Jay
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1789, 8, 25).getTime(),
                new Date(1790, 2, 22).getTime()
              ]
            }
          ]
        },
        // Edmund Randolph
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1794, 0, 2).getTime(),
                new Date(1795, 7, 20).getTime()
              ]
            }
          ]
        },
        // Timothy Pickering
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1795, 7, 20).getTime(),
                new Date(1800, 4, 12).getTime()
              ]
            }
          ]
        },
        // Charles Lee
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1800, 4, 13).getTime(),
                new Date(1800, 5, 5).getTime()
              ]
            }
          ]
        },
        // John Marshall
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1800, 5, 13).getTime(),
                new Date(1801, 2, 4).getTime()
              ]
            }
          ]
        },
        // Levi Lincoln
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1801, 2, 5).getTime(),
                new Date(1801, 4, 1).getTime()
              ]
            }
          ]
        },
        // James Madison
        {
          data: [
            {
              x: 'Secretary of State',
              y: [
                new Date(1801, 4, 2).getTime(),
                new Date(1809, 2, 3).getTime()
              ]
            }
          ]
        },
      ],
      options: {
        chart: {
          height: 350,
          type: 'rangeBar',

          toolbar: {
            show:false,
            enabled: false
          }
        },
        plotOptions: {
          bar: {
            horizontal: true,
            barHeight: '100%',
            rangeBarGroupRows: true
          }
        },
        colors: [
          "#008FFB", "#00E396", "#FEB019", "#FF4560"
        ],
        fill: {
          type: 'solid'
        },
        xaxis: {
          type: 'datetime',
        },
        yaxis: {
          show: false,
        },
        legend: {
          position: 'right',
          show: false
        },
        tooltip: {
          show:false,
          enabled: false
        },
      }
    }

  const [state, setState] = useState(initialState);


  useEffect(() => {
    getSites();
    getPlants(state.site_id);
  });

  const getSites = () => {
    let config = {
      method: "get",
      url: `http://localhost:3001/sites`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        sites: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const getPlants = (id) => {
    let config = {
      method: "get",
      url: `http://localhost:3001/plants/${id}`,
    };
    axios(config)
    .then((res) => {
      setState((prev) => ({
        ...prev,
        plants: res.data,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
  };

  return (
    <div className='App'>
      <div className='machine'>
      {state.isShowing ?
        <div className='header'>
          <form>
            <FaFilter className='icon' size={20} style={{color:"#aeb3b5"}} />
            <label>Site:
              <select  className="classic" name="site">
              {state.sites.map((site, i) => (
                <option value={site.site_id}>{site.site_name}</option>
              ))}
              </select>
            </label>
            <label>Plant:
              <select className="classic" name="plant">
              {state.plants.map((plant, i) => (
                <option value={plant.plant_id}>{plant.plant_name}</option>
              ))}
              </select>
            </label>
            <label>Department:
              <select className="classic" name="department">
                <option value='a'>A</option>
                <option value='b'>B</option>
              </select>
            </label>
            <label>Work Center:
              <select className="classic" name="work_center">
                <option value='a'>A</option>
                <option value='b'>B</option>
              </select>
            </label>
            <label>Workstation:
              <select className="classic" name="workstation">
                <option value='a'>A</option>
                <option value='b'>B</option>
              </select>
            </label>
            <button className='search-button' type='submit'> Search </button>
            <FaFileExport size={20} style={{color:'#22e38c', cursor:"pointer"}} />
            <button className='calendar-button' type='submit'> {today} </button>
          </form>
        </div>
        : "" }
        <div className='content'>
        {state.isShowing ?
          <a className="close" onClick={()=> {setState((prev) => ({
            ...prev, isShowing: false
            }))}}>
            &times;
          </a>
          :
          <a className="filter" >
            <FaFilter className='icon' size={15} style={{color:"white", cursor:"pointer"}} onClick={()=> {setState((prev) => ({
              ...prev, isShowing: true
            }))}} />
          </a>

        }
          <div className='content-data'>
            <div className="data">
              <div className="left">
                <div className='name'>
                Geminis 1860
                </div>
                <div className="image">
                  <img src={Logo} alt="logo" />
                </div>
              </div>
              <div className="right">
                <div className="utilization">
                  Utilization 30%
                </div>
                <div className="graphic">
                  <ReactApexChart options={state.options} series={state.series} type="rangeBar" width={400} height={150} />
                </div>
              </div>
            </div>
          </div>
          <div className='footer'>
            <div className="info">
              <div className="indicator">
                <svg width="20" height="20">
                  <circle cx="10" cy="10" r="6" fill="green" />
                </svg>
                <label style={{color: "green"}}>Running</label>
              </div>
              <div className="indicator">
                <svg width="20" height="20">
                  <circle cx="10" cy="10" r="6" fill="orange" />
                </svg>
                <label style={{color: "orange"}}>Idle</label>
              </div>
              <div className="indicator">
                <svg width="20" height="20">
                  <circle cx="10" cy="10" r="6" fill="red" />
                </svg>
                <label style={{color: "red"}}>Down</label>
              </div>
              <div className="indicator">
                <svg width="20" height="20">
                  <circle cx="10" cy="10" r="6" fill="black" />
                </svg>
                <label style={{color: "black"}}>Offline</label>
              </div>
            </div>
            <div className="pagination">
              <span><IoPlaySkipBackSharp style={{color:"#045075", cursor:"pointer"}} onClick={()=> {setState((prev) => ({
                ...prev, page: 1
              }))}} /></span>
              <span><IoCaretBackSharp style={{color:"#045075", cursor:"pointer"}} onClick={()=> {setState((prev) => ({
                ...prev, page: state.page !== 1 ? state.page-1 : state.page
              }))}} /></span>
              <span><input type="text" value={state.page +'/'+ state.maxPage} disabled="disabled"/></span>
              <span><IoCaretForwardSharp style={{color:"#045075", cursor:"pointer"}} onClick={()=> {setState((prev) => ({
                ...prev, page: state.page !== 5 ? state.page+1 : state.page
              }))}} /></span>
              <span><IoPlaySkipForwardSharp style={{color:"#045075", cursor:"pointer"}} onClick={()=> {setState((prev) => ({
                ...prev, page: state.maxPage
              }))}} /></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Machine;

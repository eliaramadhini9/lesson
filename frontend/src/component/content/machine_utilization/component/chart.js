import React from "react";
import { render } from "react-dom";
import { TimeSeries } from "pondjs";
import {
  Resizable,
  Charts,
  ChartContainer,
  ChartRow,
  YAxis,
  LineChart
} from "react-timeseries-charts";
import data from "./data";

class Baselines extends React.Component {
  state = {
    tracker: null
  };

  handleTrackerChanged = tracker => {
    this.setState({ tracker });
  };

  handleTimeRangeChange = timerange => {
    this.setState({ timerange });
  };

  render() {
    const series = new TimeSeries({
      name: "USD_vs_EURO",
      columns: ["time", "value"],
      points: data.widget[0].data.reverse()
    });

    const style = {
      value: {
        stroke: "#a02c2c",
        opacity: 0.2
      }
    };

    const baselineStyle = {
      line: {
        stroke: "steelblue",
        strokeWidth: 1
      }
    };

    const axisStyle = {
      labels: { labelColor: "Red", labelWeight: 100, labelSize: 11 },
      axis: { axisColor: "Orange" }
    };

    return (
      <Resizable>
        <ChartContainer timeRange={series.range()} timeAxisStyle={axisStyle}>
          <ChartRow height="150">
            <YAxis
              style={axisStyle}
              id="price"
              label="Price ($)"
              min={series.min()}
              max={series.max()}
              width="60"
              format="$,.1f"
            />
            <Charts>
              <LineChart axis="price" series={series} style={style} />
            </Charts>
          </ChartRow>
        </ChartContainer>
      </Resizable>
    );
  }
}

class App extends React.Component {
  state = {};

  render() {
    return (
      <div className="p-3 m-4 border border-muted">
        <Baselines />
      </div>
    );
  }
}

render(<App />, document.getElementById("root"));

import React from "react";
import ReactExport from "react-export-excel";

function ExportToExcel({data}) {

    const ExcelFile = ReactExport.ExcelFile;
    const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
    const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

    return (
        <ExcelFile>
            <ExcelSheet data={data} name="Transactions">
                <ExcelColumn label="Timestamp" value="timestamp" />
                <ExcelColumn label="Value" value="value" />
            </ExcelSheet>
        </ExcelFile>
    )
}

export default ExportToExcel;

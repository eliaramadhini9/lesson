import React, { useEffect, useState } from "react";
import { TimeSeries } from "pondjs";
import ReactApexChart from "react-apexcharts";
import axios from "axios";

function MachineStatus({ asset_id, date }) {
    let utilization = 0
    const initialGraphState = {
        utilization: '',
        dates: date,
        series: [
            {
                data: [],
            },
        ],
        options: {
            chart: {
                type: "rangeBar",
                toolbar: {
                    show: false,
                    enabled: false,
                },
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                    barHeight: "100%",
                    rangeBarGroupRows: true,
                },
            },
            colors: [function(opts) {                
                const values = opts.seriesIndex;
                const state = opts.w.globals.seriesNames[values];
                
                 return (state === "Running" ? "#008000": state === "Idle" ? "#ffa500" : state === "Down" ? "#ff0000" : "#000000") }],
            fill: {
                type: "solid",
            },
            xaxis: {
                type: "datetime",
                min:'',
                max:'',
                labels: {
                    datetimeUTC: false
                }
            },
            yaxis: {
                show: false,
            },
            legend: {
                show: false,
            },
            tooltip: {
                enabled: true,
                custom: (opts) => {
                    const fromYear = new Date(opts.y1).toLocaleString("en-ZA");
                    const toYear = new Date(opts.y2).toLocaleString("en-ZA");
                    const values = opts.seriesIndex;
                    const state = opts.w.globals.seriesNames[values];
                    
                    return (
                        '<div class="arrow_box">' +
                        "<span>" +
                        "State: " + state + '<br />' +
                        fromYear + " - " + toYear + 
                        "</span>" +
                        "</div>"
                      );
                },
            },
        },
    };

    const [graph, setGraph] = useState(initialGraphState);

    const getTimes = (id, date) => {
        let config = {
            method: "get",
            url: `http://localhost:3001/times/${id}/${date}`,
        };
        axios(config)
            .then((res) => {
                if (res.data.length > 0) {
                    let sum = 0
                    const x = "timeline";
                    const coba = res.data.filter((a, i) => a.value === res.data[i].value)
                    const newSeries = res.data.map((el) => {
                        if (el.value === 1) {
                            sum += 
                            new Date(new Date(el.end_time).toLocaleString("en-ZA")).getTime()
                            -
                            new Date(new Date(el.start_time).toLocaleString("en-ZA")).getTime()
                        }
                        return {
                            name: el.value === 1 ? "Running" : el.value === 2 ? "Idle" : el.value === 3 ? "Down" : "Offline",
                            data: [
                                {
                                    x,
                                    y: [
                                        new Date(el.start_time).getTime(),
                                        new Date(el.end_time).getTime(),
                                    ],
                                },
                            ],
                        }
                    });
                    utilization = sum / (1 * 24 * 3600 * 1000)
                    console.log(coba)
                    setGraph((prev) => ({
                        ...prev,
                        series: newSeries,
                        options: {
                            xaxis: {
                                min: new Date(new Date(date).setHours(0,0,1)).getTime(),
                                max: new Date(new Date(date).setHours(23,59,59)).getTime(),
                            }
                        },
                        utilization: utilization
                    }));
                } else {
                    setGraph((prev) => ({
                        ...prev,
                        series: [
                            {
                                data: [],
                            },
                        ],
                    }));
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        getTimes(asset_id, date);
    }, [asset_id, date]);

    return (        
        <ReactApexChart
            options={graph.options}
            series={graph.series}
            type='rangeBar'
            width='300%'
            height='100%'
        />
    );
}

function Utilization({ asset_id, date }) {
    const initialState = {
        utilization: 0,
    };

    const [state, setState] = useState(initialState);

    const getTimes = (id, date) => {
        let config = {
            method: "get",
            url: `http://localhost:3001/times/${id}/${date}`,
        };
        axios(config)
            .then((res) => {
                if (res.data.length > 0) {
                    let sum = 0
                    res.data.map((el) => {
                        if (el.value === 1) {
                            sum += 
                            new Date(new Date(el.end_time).toLocaleString("en-ZA")).getTime()
                            -
                            new Date(new Date(el.start_time).toLocaleString("en-ZA")).getTime()
                        }
                    });
                    let percent = sum / (1 * 24 * 3600 * 1000)
                    let utilization = percent !== 0 ? percent.toFixed(2) : percent;
                    setState((prev) => ({
                        ...prev,
                        utilization: utilization
                    }));
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        getTimes(asset_id, date);
    }, [asset_id, date]);

    return " " + state.utilization
}

export { MachineStatus, Utilization};

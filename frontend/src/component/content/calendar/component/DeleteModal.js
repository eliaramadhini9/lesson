function DeleteModal({close, deleteNotes}) {
return(
  <div className="modal">
    <div className="content">
      Delete notes?
    </div>
    <div className="button-area">
      <button className="button" form='notesUpdate' onClick={deleteNotes}>
        Confirm
      </button>
      <button className="button" onClick={()=> {
        close();
        }}
        >
        Cancel
      </button>
    </div>
  </div>
  )
}
export default DeleteModal;

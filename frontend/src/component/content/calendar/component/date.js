function daysInMonth(date) {
  const d = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  return d.getDate();
}

function firstDayOfMonth(date) {
  const d = new Date(date.getFullYear(), date.getMonth(), 0);
  return (d.getDay() === 0 ? 7 : d.getDay())
}

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

export {daysInMonth, firstDayOfMonth, months}

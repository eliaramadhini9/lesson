import { FaPlusSquare } from 'react-icons/fa'
import Popup from 'reactjs-popup';
import AddModal from './addModal.js'
import UpdateModal from './UpdateModal.js'

function DateGrid({state, setState, getNoteId, handleChange, addNotes, getDate, deleteNotes, updateNotes }) {
  const today = new Date()
  return (
    <div className='date-grid'>
      {(Array.from({ length: state.firstDayOfMonth}, (k, v) => null).concat((Array.from({ length: state.daysInMonth}, (k, v) => v + 1)))).map((day, i) =>(
      <div className='date' style={{
              background: day===today.getDate() ? "rgba(4, 80, 117, .4)" : ""}}>
        <div className='span'>
          <p id='span-1'>{day}</p>
          {day === null ? "" :
          <FaPlusSquare id='span-2' onClick={()=> getDate(day)} />
            }
        </div>
        <div className='notes'>
          {state.notes.map((note, i) =>(
          day==note.date ?
          <p style={{
                  background: note.priority == 'low' ? "green" : note.priority == 'medium' ? "yellow" : "red"}} onClick={()=> getNoteId(note.id)}>{note.title}</p>
          : ""
          ))}
        </div>
      </div>
      ))}
      <Popup open={state.addModal} onClose={()=> {setState((prev) => ({
        ...prev, addModal: false
        }))}} modal contentStyle={{
            maxWidth: "800px",
            width: '600px',
          }}>
        {(close) => (
          <AddModal close={close} state={state} handleChange={handleChange} addNotes={addNotes} />
        )}
      </Popup>

      <Popup open={state.updateModal} onClose={()=> {setState((prev) => ({
        ...prev, updateModal: false
        }))}} modal contentStyle={{
            maxWidth: "800px",
            width: '600px',
          }}>
        {(close) => (
          <UpdateModal close={close} updateNotes={updateNotes} handleChange={handleChange} state={state} deleteNotes={deleteNotes} />

        )}
      </Popup>
    </div>
  )
}

export default DateGrid;

function AddModal({close, addNotes, handleChange, state}) {
return(
  <div className="modal">
    <div className="content">
      <form id='notesInput' onSubmit={addNotes}>
        <label> Title: </label>
        <label> Date:
          <input type="text" name="dateSelected" value={state.dateSelected} onChange={handleChange} disabled="disabled" />
        </label>
        <input type="text" name="title" value={state.title} onChange={handleChange} />
        <label> Notes: </label>
        <textarea type="text" name="note" value={state.note} onChange={handleChange} />
        <label> Priority: </label>
        <select name='priority' value={state.priority} onChange={handleChange}>
          <option value='3'>High</option>
          <option value='2'>Medium</option>
          <option value='1'>Low</option>
        </select>
      </form>
      <div className='button-area'>
        <button className='button' form='notesInput' type='submit'> Add </button>
        <button className="button" onClick={()=> {
          close();
          }}> Cancel </button>
      </div>
    </div>
  </div>
  )
}
export default AddModal;

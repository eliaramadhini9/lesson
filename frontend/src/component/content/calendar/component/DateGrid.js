import { FaPlusSquare } from 'react-icons/fa'
import Popup from 'reactjs-popup';
import AddModal from './addModal.js'
import UpdateModal from './UpdateModal.js'

function DateGrid({state, setState, getNoteId, handleChange, addNotes, getDate, deleteNotes, updateNotes }) {
  const today = new Date()
  return (
    <div className='date-grid'>
      {(Array.from({ length: state.firstDayOfMonth}, (k, v) => null).concat((Array.from({ length: state.daysInMonth}, (k, v) => v + 1)))).map((day, i) =>(
      <div className='date' style={{background: day==today.getDate() && state.year == today.getFullYear() && state.month == today.getMonth() ? "rgba(4, 80, 117, .6)" : ""}}>
        <div className='span'>
          <p id='span-1' style={{color: i%7==5 || i%7==6 ? "red" : ""}}>{day}</p>
          {day === null ? "" :
          <FaPlusSquare id='span-2' onClick={()=> getDate(day)} />
            }
        </div>
        <div className='notes'>
          {state.notes.map((note, i) =>(
            day==new Date(note.date).getDate() && state.year == new Date(note.date).getFullYear() && state.month == new Date(note.date).getMonth()?
          <p style={{background: note.priority == 1 ? "green" : note.priority == 2 ? "yellow" : "red"}} onClick={()=> getNoteId(note.id)}>
            {note.title}
          </p>
          : ""
          ))}
        </div>
      </div>
      ))}
      <Popup open={state.addModal} onClose={()=> {setState((prev) => ({
        ...prev, addModal: false
        }))}} modal contentStyle={{
            maxWidth: "800px",
            width: '600px',
          }}>
        {(close) => (
          <AddModal close={close} state={state} handleChange={handleChange} addNotes={addNotes} />
        )}
      </Popup>

      <Popup nested open={state.updateModal} onClose={()=> {setState((prev) => ({
        ...prev, updateModal: false
        }))}} modal contentStyle={{
            maxWidth: "800px",
            width: '600px',
          }}>
        {(close) => (
          <UpdateModal close={close} updateNotes={updateNotes} handleChange={handleChange} state={state} deleteNotes={deleteNotes} />

        )}
      </Popup>
    </div>
  )
}

export default DateGrid;

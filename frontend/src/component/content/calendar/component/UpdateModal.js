import Popup from 'reactjs-popup';
import DeleteModal from './DeleteModal.js'

function UpdateModal({close, updateNotes, handleChange, state, deleteNotes}) {
return(
  <div className="modal">
    <div className="content">
      {state.notesSelected.map((note, i) => (
      <form id='notesUpdate' onSubmit={updateNotes}>
        <label> Title: </label>
        <label> Date:
          <input id='date' type="text" name="date" value={new Date(note.date).getFullYear()+'-'+new Date(note.date).getMonth()+'-'+new Date(note.date).getDate()} onChange={handleChange} disabled="disabled" />
        </label>
        <input type="text" name="title" value={note.title} onChange={handleChange} />
        <label> Notes: </label>
        <textarea type="text" name="note" value={note.note} onChange={handleChange} />
        <label> Priority: </label>
        <select name='priority' value={note.priority} onChange={handleChange}>
          <option value='3'>High</option>
          <option value='2'>Medium</option>
          <option value='1'>Low</option>
        </select>

      </form>
      ))}
      <div className='button-area'>
        <button className='button' form='notesUpdate' type='submit'> Update </button>
        <Popup trigger={<button className="button"> Delete </button>}
          modal
          contentStyle={{
              maxWidth: "600px",
              width: "350px",
            }}
          >
          {(close) => (
            <div className="modal">
              <div className="content">
                Delete notes?
              </div>
              <div className="button-area">
                <button className="button" form='notesUpdate' onClick={deleteNotes}>
                  Confirm
                </button>
                <button className="button" onClick={()=> {
                  close();
                  }}
                  >
                  Cancel
                </button>
              </div>
            </div>
          )}
        </Popup>
      </div>
    </div>
  </div>
  )
}
export default UpdateModal;

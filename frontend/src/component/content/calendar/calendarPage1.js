import React, { useEffect, useState} from 'react';
import axios from 'axios'
import './calendar.css';
import '../../../css/modal.css'
import DateGrid from './component/DateGrid.js'
import { daysInMonth, firstDayOfMonth, months } from './component/date.js'

function Calendar() {

  const today = new Date()
  const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

  const [date, setDate] = useState(today)

  const initialCalendarState = {
    day: date.getDate(),
    month: date.getMonth(),
    year: date.getFullYear(),
    daysInMonth: daysInMonth(date),
    firstDayOfMonth: firstDayOfMonth(date),
    notes: [],
    notesSelected: [],
    noteId: '',
    dateNote: '',
    title: '',
    priority: '',
    id_user: '',
    dateSelected: '',
    updateModal: false,
    addModal: false
  }

  const [state, setState] = useState(initialCalendarState)

  useEffect(() => {
    setState({
      ...state,
      day: date.getDate(),
      month: date.getMonth(),
      year: date.getFullYear(),
      daysInMonth: daysInMonth(date),
      firstDayOfMonth: firstDayOfMonth(date)
    })
  }, [date])

  const getNotes = () => {
    let user = localStorage.getItem('user_id')
    let config = {
      method: 'get',
      url: `http://localhost:3001/note/${user}`
    }
    axios(config)
      .then((res) => {
        setState({
          ...state,
          notes: res.data
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  const getNote = (id) => {
    let config = {
      method: 'get',
      url: `http://localhost:3001/notes/${id}`
    }
    axios(config)
      .then((res) => {
        setState({
          ...state,
          notesSelected: res.data
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  const addNotes = (e, close) => {
    e.preventDefault();

    let config = {
      method: 'post',
      url: 'http://localhost:3001/addnote',
      data: {
        title: state.title,
        note: state.note,
        date: state.dateSelected,
        user_id: parseInt(localStorage.getItem('user_id')),
        priority: state.priority
      }
    }
    axios(config)
      .then((res) => {
        setState({
          ...state,
          addModal: false
        })
        close()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const updateNotes = (e) => {
    e.preventDefault();

    let config = {
      method: 'put',
      url: `http://localhost:3001/updatenote/${state.noteId}`,
      data: {
        title: state.title,
        note: state.note,
        priority: state.priority
      }
    }
    axios(config)
      .then((res) => {
        alert(res)
        setState({
          ...state,
          updateModal: false
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  const deleteNotes = (close) => {
    alert(state.noteId)
    let config = {
      method: 'delete',
      url: `http://localhost:3001/deletenote/${state.noteId}`
    }
    axios(config)
      .then((res) => {
        close()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const getNoteId = (id) => {
    setState({
      ...state,
      noteId: id,
      updateModal: true
    });
  }

  const getDate = (id) => {
    setState({
      ...state,
      dateSelected: `${state.year}-${state.month+1}-${id}`,
      addModal: true
    });
  }

  const handleChange = (e) => {
    const {
      name,
      value
    } = e.target

    setState({
      ...state,
      [name]: value
    });
  }

  const handleChangeMonth = (e) => {
    setDate(new Date(state.year, e.target.value, state.day));
  }

  const handleChangeYear = (e) => {
    setDate(new Date(e.target.value, state.month, state.day));
  }

  return (
    <div className='App'>
      <div className='calendar'>
        <div className='header'>
          <label>Month:</label>
          <select className='classic' name='monthSelected' onChange={handleChangeMonth}>
            {months.map((month, i) => (
            <option value={i} selected={i===date.getMonth()}>{month}</option>
            ))}
          </select>
          <label>Year:</label>
          <select className='classic' name='yearSelected' onChange={handleChangeYear} value={state.yearNow}>
            {(Array.from({ length:5}, (k, v) => v + (parseInt(state.year)-2))).map((year, i) => (
            <option value={year} selected={year==date.getFullYear()}>{year}</option>
            ))}
          </select>
        </div>
        <div className='day-of-week'>
          {days.map((day, i) =>(
          <span>{day}</span>
          ))}
        </div>
        <DateGrid state={state} setState={setState} getNoteId={getNoteId} getDate={getDate} handleChange={handleChange} addNotes={addNotes}/>
      </div>
    </div>
  )
}

export default Calendar;

import React, { useEffect, useState } from "react";
import axios from "axios";
import "./calendar.css";
import "../../../css/modal.css";
import DateGrid from "./component/DateGrid.js";
import { daysInMonth, firstDayOfMonth, months } from "./component/date.js";

function Calendar() {
    const today = new Date();
    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    const days = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ];

    const [date, setDate] = useState(today);

    const initialCalendarState = {
        day: date.getDate(),
        month: date.getMonth(),
        year: date.getFullYear(),
        daysInMonth: daysInMonth(date),
        firstDayOfMonth: firstDayOfMonth(date),
        notes: [],
        notesSelected: [],
        noteId: "",
        dateNote: "",
        title: "percobaaan",
        priority: "high",
        id_user: "",
        dateSelected: "",
        updateModal: false,
        addModal: false,
    };

    const [state, setState] = useState(initialCalendarState);

    useEffect(() => {
        getNotes();
        setState((prev) => ({
            ...prev,
            day: date.getDate(),
            month: date.getMonth(),
            year: date.getFullYear(),
            daysInMonth: daysInMonth(date),
            firstDayOfMonth: firstDayOfMonth(date),
        }));
    }, [date]);

    const getNotes = () => {
        let user = localStorage.getItem("user_id");
        let config = {
            method: "get",
            url: `http://localhost:3001/note/${user}`,
        };
        axios(config)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    notes: res.data,
                }));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getNote = (id) => {
        let config = {
            method: "get",
            url: `http://localhost:3001/notes/${id}`,
        };
        axios(config)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    notesSelected: res.data,
                    noteId: id
                }));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const addNotes = (e, close) => {
      e.preventDefault();

      let config = {
        method: "post",
        url: "http://localhost:3001/addnote",
        data: {
          title: state.title,
          note: state.note,
          date: state.dateSelected,
          user_id: parseInt(localStorage.getItem("user_id")),
          priority: parseInt(state.priority),
        },
      };
      axios(config)
        .then((res) => {
          setState({
            ...state,
            addModal: false,
          });
          getNotes()
          close();
        })
        .catch((err) => {
          console.log(err);
        });
    };

    const updateNotes = (e) => {
      e.preventDefault()
      let config = {
        method: "put",
        url: `http://localhost:3001/updatenote/${state.noteId}`,
        data: {
          title: state.title,
          note: state.note,
          priority: parseInt(state.priority),
        },
      };
      axios(config)
        .then((res) => {
          getNotes()
          setState({
            ...state,
            updateModal: false,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    };

    const deleteNotes = () => {
        let config = {
            method: "delete",
            url: `http://localhost:3001/deletenote/${state.noteId}`,
        };
        axios(config)
            .then((res) => {
                getNotes()
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getNoteId = (id) => {
        setState((prev) => ({
            ...prev,
            noteId: id,
            updateModal: true,
        }));
        getNote(id)
    };

    const getDate = (id) => {
        setState({
            ...state,
            dateSelected: `${state.year}-${state.month + 1}-${id}`,
            addModal: true,
        });
    };

    const handleChange = (e) => {
        const { name, value } = e.target;


        setState((prev) => ({
            ...prev,
            [name]: value
        }));
    };

    const handleChangeMonth = (e) => {
        setDate(new Date(state.year, e.target.value, state.day));
    };

    const handleChangeYear = (e) => {
        setDate(new Date(e.target.value, state.month, state.day));
    };

    return (
        <div className='App'>
            <div className='calendar'>
                <div className='header'>
                    <label>Month:</label>
                    <select
                        className='classic'
                        name='monthSelected'
                        onChange={handleChangeMonth}>
                        {months.map((month, i) => (
                            <option value={i} selected={i === date.getMonth()}>
                                {month}
                            </option>
                        ))}
                    </select>
                    <label>Year:</label>
                    <select
                        className='classic'
                        name='yearSelected'
                        onChange={handleChangeYear}
                        value={state.yearNow}>
                        {Array.from(
                            { length: 5 },
                            (k, v) => v + (parseInt(state.year) - 2)
                        ).map((year, i) => (
                            <option
                                value={year}
                                selected={year == date.getFullYear()}>
                                {year}
                            </option>
                        ))}
                    </select>
                </div>
                <div className='day-of-week'>
                    {days.map((day, i) => (
                        <span>{day}</span>
                    ))}
                </div>
                <DateGrid
                    state={state}
                    setState={setState}
                    getNoteId={getNoteId}
                    getDate={getDate}
                    handleChange={handleChange}
                    addNotes={addNotes}
                    deleteNotes={deleteNotes}
                    updateNotes={updateNotes}
                />
            </div>
        </div>
    );
}

export default Calendar;

import Header from '../../Header.js'
import CalendarPage from './calendarPage.js'
// import './homeStyle.css';

function Home() {
  return (
    <div className='container'>
      <Header />
      <CalendarPage />
    </div>
  )
}

export default Home;

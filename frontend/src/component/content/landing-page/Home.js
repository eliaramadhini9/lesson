import Header from '../../Header.js'
import HomePage from './homePage.js'
import './homeStyle.css';

function Home() {
  return (
    <div className='container'>
      <Header />
      <HomePage />
    </div>
  )
}

export default Home;

import React from 'react';
import './homeStyle.css';

function Home() {
  return (
    <div className='App'>
      <div className='Box'>
        <a href='/machine_utilization'>Machine Utilization</a>
        <a href='/calendar'>Calendar</a>
        <a href='/currency'>Currency</a>
      </div>
    </div>
  )
}

export default Home;

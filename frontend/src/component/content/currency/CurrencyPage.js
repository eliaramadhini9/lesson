import React, { useEffect, useState} from 'react';
import Popup from 'reactjs-popup';
import axios from 'axios'
import './currency.css';
import '../../../css/modal.css';
import DeleteModal from './component/DeleteModal.js'
import ChartModal from './component/ChartModal.js'

function Currency() {

  const initialDataState = {
    dataCurrency: [],
    dataSelected: '',
    id: '',
    country: '',
    value: '',
    labelHours: [],
    labelDays: ['Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    dataHours: [],
    dataDays: [],
    open: false
  }

  const [state, setState] = useState(initialDataState)

  useEffect(() => {
    getAll()
  })

  const getAll = () => {
    let config = {
      method: 'get',
      url: 'http://localhost:3001/currency'
    }
    axios(config)
      .then((res) => {
        setState({
          ...state,
          dataCurrency: res.data
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  const create = (e) => {
    e.preventDefault();

    let config = {
      method: 'post',
      url: 'http://localhost:3001/currency',
      data: {
        country: state.country,
        value: state.value
      }
    }
    axios(config)
      .then((res) => {
        getAll()
        state.country = ''
        state.value = ''
      })
      .catch(err => {
        console.log(err)
      })
  }

  const deleteData = (close) => {
    let config = {
      method: 'delete',
      url: `http://localhost:3001/currency/${state.id}`
    }
    axios(config)
      .then((res) => {
        close()
        getAll()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const handleChange = (event) => {
    const {
      name,
      value
    } = event.target
    setState({
      ...state,
      [name]: value
    });
  }

  const getId = (id) => {
    setState({
      ...state,
      id: id
    });
  }

  const getChart = (id) => {
    let selectedData = state.dataCurrency.filter((i) => i.id === id)
    let max = 1.2 * selectedData[0].value
    let min = 0.8 * selectedData[0].value

    let setLabelHours = []
    let setDataHours = []
    let setDataDays = []

    for (var i = 0; i < 25; i += 3) {
      setLabelHours.push(i+':00')
      setDataHours.push(Math.floor(Math.random() * (max - min + 1) + min))
    }

    for (var j = 0; j < state.labelDays.length; j++) {
      setDataDays.push(Math.floor(Math.random() * (max - min + 1) + min))
    }

    setState({
      ...state,
      dataSelected: selectedData[0],
      labelHours: setLabelHours,
      dataHours: setDataHours,
      dataDays: setDataDays,
      open: true
    })
  }

  const chartPerHours = {
    labels: state.labelHours,
    datasets: [{
      label: 'Hours',
      backgroundColor: '#045075',
      borderColor: 'rgba(0,0,0,1)',
      borderWidth: 1,
      data: state.dataHours
    }]
  }

  const chartPerDays = {
    labels: state.labelDays,
    datasets: [{
      label: 'Days',
      backgroundColor: 'rgba(75,192,192,1)',
      borderColor: 'rgba(0,0,0,1)',
      borderWidth: 1,
      data: state.dataDays
    }]
  }

  return (
    <div className='App'>
      <div className='box'>
        <table className='table'>
          <thead>
            <tr>
              <th style={{width:'50px'}}>No.</th>
              <th>Country</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            {state.dataCurrency.map((list, i) => (
            <tr key={i} style={{
                  background: state.id === list.id ? "#676e70" : ""
                }} onDoubleClick={()=> getChart(list.id)} onClick={() => getId(list.id)}>
              <td style={{width:'50px'}}>{i+1}</td>
              <td className='left'>{list.country}</td>
              <td className='right'>{list.value}</td>
            </tr>
            ))}
          </tbody>
        </table>
        <div className='form'>
          <form id='currencyInput' onSubmit={create}>
            <label> Country:</label>
            <input id='cInput' type="text" name="country" value={state.country} onChange={handleChange} />
            <label>Value:</label>
            <input type="text" name="value" value={state.value} onChange={handleChange} />
          </form>
          <button className='button' form='currencyInput' type='submit'> Add </button>
          <Popup trigger={<button className="button"> Delete </button>}
            modal
            contentStyle={{
                maxWidth: "600px",
                width: "30%",
              }}
            >
            {(close) => (
              <DeleteModal deleteData={deleteData} close={close} />
            )}
          </Popup>
          <Popup open={state.open} onClose={()=> {setState((prev) => ({
            ...prev, open: false
          }))}} modal contentStyle={{
                maxWidth: "800px",
                width: "40%",
              }}>
            {close => (
              <ChartModal state={state} close={close} chartPerDays={chartPerDays} chartPerHours={chartPerHours} />
            )}
          </Popup>
        </div>
      </div>
    </div>
  )
}

export default Currency;

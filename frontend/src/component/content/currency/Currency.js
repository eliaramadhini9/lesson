import Header from '../../Header.js'
import CurrencyPage from './CurrencyPage.js'

function Currency() {
  return (
    <div className='container'>
      <Header />
      <CurrencyPage />
    </div>
  )
}

export default Currency;

function DeleteModal({close, deleteData}) {
  return(
    <div className="modal">
      <div className="content">
        Are you sure?
      </div>
      <div className="actions">
        <button className="button-modal" form='currencyInput' onClick={()=> deleteData(close)}>
          Confirm
        </button>
        <button className="button-modal" onClick={()=> {
          close();
          }}
          >
          Cancel
        </button>
      </div>
    </div>
  )
}

export default DeleteModal;

import { Line } from "react-chartjs-2";

function ChartModal({close, state, chartPerHours, chartPerDays}) {
  return(
    <div className="modal">
      <a className="close" onClick={()=> close()}>
        &times;
      </a>
      <div className="header">{state.dataSelected.country} Currency</div>
      <div className="content">
        <div className='chart'>
          <Line data={chartPerHours} />
        </div>
      </div>
      <div className="content">
        <div className='chart'>
          <Line data={chartPerDays} />
        </div>
      </div>
    </div>
  )
}

export default ChartModal;

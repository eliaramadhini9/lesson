import React from 'react'
import { useLocation } from 'react-router-dom';
import Logo from '../image/cadit.jpg';
import '../css/style.css';


function Header() {
  const location = useLocation()
;  return (
    <div className='main'>
      <div className='header'>
        <h2>{location.pathname === "/currency" ? "Historical Currency" : location.pathname === "/calendar" ? "Calendar" : location.pathname === "/machine_utilization" ? "Machine Utilization" : ""}</h2>
      </div>
    </div>
  )
}


export default Header;

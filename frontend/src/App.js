import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Switch,
} from "react-router-dom";
import Header from "./component/Header.js";
import Sidebar from "./component/Sidebar.js";
import Login from "./component/content/login/loginPage.js";
import Home from "./component/content/landing-page/homePage.js";
import Currency from "./component/content/currency/CurrencyPage.js";
import Calendar from "./component/content/calendar/calendarPage.js";
import Machine from "./component/content/machine_utilization/machinePage.js";
import Maps from "./component/content/map/mapPage.js";
import './css/style.css';

function App() {
    return (
        <div className='container'>
            <Router>
            {localStorage.getItem("isLoged") !== null || localStorage.getItem("isLoged") === "true" ?
                <Sidebar />
                :
                ""
            }
                <div className="content">
                    <Header />
                    {localStorage.getItem("isLoged") === null ||
                        localStorage.getItem("isLoged") === "false" ? (
                        <Route>
                            <Redirect to='/login' />
                            <Route path='/login' component={Login} />
                        </Route>
                    ) : (
                        <Switch>
                            <Route exact path='/currency' component={Currency} />
                            <Route path='/calendar' component={Calendar} />
                            <Route path='/machine_utilization' component={Machine} />
                            <Route path='/map' component={Maps} />
                        </Switch>
                    )}
                </div>
            </Router>

        </div>
    );
}

export default App;

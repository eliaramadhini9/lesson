import React from "react"
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Link
} from "react-router-dom";
import { AiOutlineHome } from "react-icons/ai";
import VehicleMap from "./component/content/Map/Tracking/VehicleMap.js";
import Map from "./component/content/Map/TagMap/Map.js";
import LandingPage from "./component/content/Map/landingPage.js";
import './css/style.css';

function App() {
    return (       
        <div className="container">
        <Router>
            <Link className="linkTo" to='/'><div className="back">
                <AiOutlineHome size={25} />
            </div></Link>
                <Switch>
                    <Route exact path='/' component={LandingPage} />
                    <Route path='/tracking' component={VehicleMap} />
                    <Route path='/tag_map' component={Map} />
                </Switch>
            </Router>
        </div>
    );
}

export default App;

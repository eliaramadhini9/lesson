import React, {useState} from "react"
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Switch,
} from "react-router-dom";
import Maps from "./component/content/map/mapPage.js";
import Table from "./component/content/map/table.js";
import AddForm from "./component/content/map/addForm.js";
import EditForm from "./component/content/map/editForm.js";
import './css/style.css';
import LandingPage from "./component/content/map/landingPage.js";

function App() {
    const [state, setState] = useState(false)
    const [position, setPosition] = useState(null)
    const [getId, setGetId] = useState(null)
    const [polyLine, setPolyLine] = useState([]);

    return (

       
        <div className="container">
             <Router>
                    <Switch>
                        <Route path='/' component={LandingPage} />
                    </Switch>
            </Router>
            {/* <Maps show={setState} hide={state} getPosition={setPosition} getId={setGetId} polyLine={polyLine} />
            <Table getId={getId} polyLine={setPolyLine} />
            <AddForm show={state} hide={setState} getPosition={position}/> */}
            {/* <EditForm /> */}
        </div>
    );
}

export default App;

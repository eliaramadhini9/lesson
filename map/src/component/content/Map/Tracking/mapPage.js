import React, { useEffect } from "react";
import { MapContainer, TileLayer, Marker, Tooltip, Polyline } from 'react-leaflet'
import L from "leaflet";
import Marker_icon from "leaflet/dist/images/marker-icon.png"
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import {vehicle} from './component/vehicle.js'
import "./css/map.css";
import "../../../../css/style.css";
import 'leaflet/dist/leaflet.css';

function Maps({getId, polyLine}) {
  useEffect(() => {
    delete L.Icon.Default.prototype._getIconUrl;
    L.Icon.Default.mergeOptions({
      iconUrl: Marker_icon,
      shadowUrl: iconShadow
    });
  }, []);

  return (
    <MapContainer className="map" center={[48.86, 2.36]} zoom={13} scrollWheelZoom={false}>
      <TileLayer attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
      {vehicle.map((a) => (
      <Marker position={a.position}>
        <Tooltip onOpen={()=> getId(a.id)} >{a.vehicle}<br/>{a.position[0] + ', ' + a.position[1]}<br/>{a.timestamp}<br/></Tooltip>
      </Marker>
        ))}
      <Polyline positions={polyLine} />
    </MapContainer>
  )
}

export default Maps;

import React, {useState} from "react"
import Maps from "./mapPage.js";
import Table from "./component/table.js";
import '../../../../css/style.css';

function VehicleMap() {
    const [state, setState] = useState(false)
    const [getId, setGetId] = useState(null)
    const [polyLine, setPolyLine] = useState([]);

    return (
        <div className="container">
            <Maps show={setState} hide={state} getId={setGetId} polyLine={polyLine} />
            <Table getId={getId} polyLine={setPolyLine} />
        </div>
    );
}

export default VehicleMap;

import { AiOutlineDoubleLeft, AiOutlineDoubleRight, AiFillCloseCircle } from "react-icons/ai";
import React, { useState } from "react";
import Vehicle from "../../../../../image/vehicle.jpg";
import {vehicle} from '../component/vehicle.js'
import "../css/table.css";

function Table({getId, polyLine}) {
  const initialDataState = {
    isShowing: true,
    getDetails: false,
    id: '',
    detail: false,
    data: [],
    vehicledata: null
  }

  const [state, setState] = useState(initialDataState)

  const getID = (id, vehicleId) => {
    let getDetail = vehicle.filter(a => a.id === id)
    let getDetails = vehicle.filter(a => a.vehicle === vehicleId)
    setState({
      ...state,
      id: id,
      data: getDetail,
      vehicledata: getDetails,
      getDetails: true
    });
    polyLine(getDetails.reduce((prev, curr) => {
      prev.push(curr.position);
      
      return prev;
    },[]))
  }

  return (
    <div className="table-container table-main">
    {state.getDetails? 
    <div className="toggle-close-detail">
      <AiFillCloseCircle onClick={() => { setState((prev) => ({...prev, getDetails: false, id:'', vehicledata: null })); polyLine([]) }} />
    </div> : null }
      {state.getDetails? 
      <div className='detail-main'>
        <div className='image'>
          <img className="img" src={Vehicle} alt='logo'/>
        </div>
        <div className="detail">
          {state.data.map((a,i)=>( 
          <table className="det">
            <tr>
              <th>Vehicle ID</th>
              <td>{a.vehicle}</td>
            </tr>
            <tr>
              <th>Status</th>
              <td>Running</td>
            </tr>
            <tr>
              <th>Driver</th>
              <td>Orang</td>
            </tr>
            <tr>
              <th>Fuel</th>
              <td>90%</td>
            </tr>
          </table>
          ))}
        </div>
      </div>
      : null }
      {state.isShowing ? 
      <table className='table'>
        <thead>
          <tr>
            <th>Vehicle ID</th>
            <th>Longitude</th>
            <th>Latitude</th>
            <th>Timestamp</th>
          </tr>
        </thead>
        <tbody>
          {(state.vehicledata === null ? vehicle : state.vehicledata).map((list, i) => (
          <tr key={i} style={{
                background: state.id === list.id || getId === list.id ? "rgba(4, 80, 117, .2)" : getId === list.id ? "rgba(4, 80, 117, .2)" : "",
                border: state.id === list.id ? "2px solid #045075" : ""
              }} onClick={() => getID(list.id, list.vehicle)}>
            <td>{list.vehicle}</td>
            <td>{list.position[1]}</td>
            <td>{list.position[0]}</td>
            <td>{list.timestamp}</td>
          </tr>
          ))}
        </tbody>
      </table>
      : null }
      <div className="toggle-show-hide">
        {state.isShowing ? 
        <AiOutlineDoubleRight onClick={() => { setState((prev) => ({...prev,isShowing: false}))}} />
        :
        <AiOutlineDoubleLeft onClick={() => { setState((prev) => ({...prev,isShowing: true}))}} />
        }
     </div> 
    </div>
  );
};

export default Table


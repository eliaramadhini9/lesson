const vehicle = [
    {
      id: 1,
      vehicle: 'DS1234',
      position: [48.86, 2.29],
      timestamp: "2021/05/05 13:01:00"
    },
    {
      id: 2,
      vehicle: 'SD1234',
      position: [48.86, 2.336],
      timestamp: "2021/05/05 13:01:00"
    },
    {
      id: 3,
      vehicle: 'TS1234',
      position: [48.85, 2.35],
      timestamp: "2021/05/05 13:01:00"
    },
    {
      id: 4,
      vehicle: 'DS1234',
     position: [48.86, 2.33],
      timestamp: "2021/05/05 13:01:00"
    },
    {
      id: 5,
      vehicle: 'DS1234',
     position: [48.84, 2.32],
      timestamp: "2021/05/05 13:01:00"
    },
    {
      id: 6,
      vehicle: 'TS1234',
      position: [48.89, 2.35],
      timestamp: "2021/05/05 13:01:00"
    },
  ]

  export {vehicle}
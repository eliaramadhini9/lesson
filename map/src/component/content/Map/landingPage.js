import React from "react";
import { Link } from "react-router-dom";
import "./landingPage.css";

export default function LandingPage() {
    
    return (
        <div className="landingpage-main">
            <div className="landingpage" />
            <div className="box">
                <div className="header">Menu</div>
                <Link className="linkTo" to="/tracking">
                    <div className="menu">Tracking</div>
                </Link>
                <Link className="linkTo" to="/tag_map">
                    <div className="menu">Tag Map</div>
                </Link>
            </div>
        </div>
    )
};

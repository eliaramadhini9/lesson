import React, { useEffect, useState } from "react";
import { FaRegTrashAlt } from "react-icons/fa";
import { MapContainer, TileLayer, Marker, Tooltip, Popup } from 'react-leaflet'
import L from "leaflet";
import Marker_icon from "leaflet/dist/images/marker-icon.png"
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import AddMarker from './component/addMarker.js'
import DeleteModal from './component/delete-modal.js'
import "./css/map.css";
import "../../../../css/style.css";
import 'leaflet/dist/leaflet.css';
import "../../../../css/delete-modal.css";

function Maps({show, hide, getPosition, dataPin, showPreview, getId}) {
  const [open, setOpen] = useState(false) 
  useEffect(() => {
    delete L.Icon.Default.prototype._getIconUrl;
    L.Icon.Default.mergeOptions({
      iconUrl: Marker_icon,
      shadowUrl: iconShadow
    });
  }, []);

  const get = (id) =>{
    showPreview(true);
    getId(id)
  } 

  
  const del = (id, close) =>{
    dataPin.splice(id, 1)
    close()
  } 

  return (
    <MapContainer className="map" center={[48.86, 2.36]} zoom={13} scrollWheelZoom={false}>
      <TileLayer attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
      {dataPin.map((a,i) => (
      <Marker key={i} position={a.position}>
        <Tooltip>{a.location}<br/>{a.position[0] + ', ' + a.position[1]}<br/></Tooltip>
        <Popup minWidth={90}>
          <span onClick={()=>setOpen(true)} style={{cursor: "pointer"}}>
            <FaRegTrashAlt size={15} />
            <DeleteModal open={open} setOpen={setOpen} i={i} del={del} />      
          </span><br /><br />
          <span onClick={()=>get(i)} style={{cursor: "pointer"}}>
            Click for more details
          </span>
        </Popup>
      </Marker>
        ))}
      <AddMarker show={show} hide={hide} getPosition={getPosition} />
    </MapContainer>
  )
}

export default Maps;

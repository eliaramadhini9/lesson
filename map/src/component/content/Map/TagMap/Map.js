import React, {useState} from "react"
import Maps from "./mapPage.js";
import AddForm from "./component/addForm";
import Preview from "./component/preview.js";
import '../../../../css/style.css';

function App() {
    const [state, setState] = useState(false)
    const [position, setPosition] = useState(null)
    const [pin, setPin] = useState([])
    const [preview, setPreview] = useState(false)
    const [getId, setGetId] = useState('')

    return (
        <div className="container">
            <Maps show={setState} hide={state} getPosition={setPosition} dataPin={pin} showPreview={setPreview} getId={setGetId} />
            <AddForm show={state} hide={setState} getPosition={position} setDataPin={setPin} dataPin={pin} />
            {/* <EditForm /> */}
            <Preview showPreview={preview} hidePreview={setPreview} setDataPin={setPin} dataPin={pin} getId={getId}/>
        </div>
    );
}

export default App;

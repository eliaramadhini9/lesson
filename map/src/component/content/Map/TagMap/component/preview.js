import { AiFillCloseCircle } from "react-icons/ai";
import React, { useState, useEffect } from "react";
import Image from './../../../../../image/upload.png';
import "../css/form.css";

export default function Preview({showPreview, hidePreview, dataPin, setDataPin, getId}) {
  const [editMode, setEditMode] = useState(false)
  const initialState = {
    isShowing: true,
    loc: '',
    lat: '',
    lng: '',
    desc: '',
    id: '',
    position: [],
    getId:''
  };

  const [state, setState] = useState(initialState);
  
  const selectedData = (id) => {
    let thisData = dataPin.filter((a,i) => i === id)
    if (thisData.length !== 0) {
      setState((prev) => ({
        ...prev,
        id: thisData[0].id,
        loc: thisData[0].location,
        lat: thisData[0].position[0],
        lng: thisData[0].position[1],
        desc: thisData[0].desc
      }))
    }
  }

  useEffect(() => {
    selectedData(getId)
  }, [getId]);

  const handleChange = (e) => {
    const {
      name,
      value
    } = e.target;
    setState(() => ({
      ...state,
      [name]: value
    }));
  }

  
  const save = (e) => {
    e.preventDefault()
    
    let data = dataPin
    let save = {
      id: state.id,
      location: state.loc,
      position: [state.lat, state.lng],
      desc: state.desc
    }
    data[getId]=save
    setDataPin(data)
}

  return showPreview ? (
    <div className="toggle toggle-active">
      <div className="toggle-close">
        <AiFillCloseCircle onClick={() => {hidePreview(false); setEditMode(false)}} />
      </div>
      <div className="preview-picture">
        <img src={Image} />
      </div>
      <div className="preview-description">
        <form id="form" className={editMode ? "form-edit" : "form-preview"} onSubmit={save}>
          <label for="loc"> Location Name: </label>
          <input id="loc" type="text" value={state.loc} name="loc" onChange={handleChange} required="required" disabled={!editMode} />
          <label for="lat"> Latitude: </label>
          <input id="lat" type="text" value={state.lat} name="lat" onChange={handleChange} disabled />
          <label for="lng"> Longitude: </label>
          <input id="lng" type="text" value={state.lng} name="lng" onChange={handleChange} disabled />
          <label for="desc"> Description: </label>
          <input id="desc" type="text" value={state.desc} name="desc" onChange={handleChange} disabled={!editMode} />
          {editMode ?
          <button className="save" form="form" type="submit">Save</button>
          :
          <button className="edit" type="button" onClick={(e) => {e.preventDefault();setEditMode(true)}}>Edit</button>
          }
        </form>
      </div>
    </div>
    ) : null 
};

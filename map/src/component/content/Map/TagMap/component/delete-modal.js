import Popup from 'reactjs-popup';

export default function DeleteModal({open, setOpen, i, del}) {

    return (
        <Popup open={open} onClose={()=> {setOpen(false)}} modal contentStyle={{
            maxWidth: "800px",
            width: "40%",
          }}>
        {(close) => (
            <div className="modal">
                <div className="content">
                    Are you sure to delete this marker?
                </div>
                <div className="actions">
                    <button className="button-modal" form='currencyInput' onClick={()=> del(i,close)}>
                        Delete
                    </button>
                    <button className="button-modal" onClick={()=> {close()}}>
                        Cancel
                    </button>
                </div>
            </div>
        )}
        </Popup>
    )
}
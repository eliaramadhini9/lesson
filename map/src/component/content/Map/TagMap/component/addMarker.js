import React, { useState } from "react";
import { Marker, Popup, useMapEvents } from 'react-leaflet'

const AddMarker = ({show, getPosition, hide}) => {
  const [position, setPosition] = useState(null);

  console.log(hide)

  useMapEvents({
    click: (e) => {
      setPosition(e.latlng);
      getPosition(e.latlng);
    },
  });

  return position === null ? null : (
    <Marker position={position}>
      <Popup minWidth={90}>
        <span onClick={()=>show(true)} style={{cursor: "pointer"}}>
          Click for add information
        </span>
      </Popup>
    </Marker>
  );
};

export default AddMarker;

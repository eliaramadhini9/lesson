import { AiFillCloseCircle } from "react-icons/ai";
import React, { useEffect, useState } from "react";
import Image from './../../../../../image/upload.png'
import "../css/map.css";

export default function AddForm({show, hide, getPosition, dataPin, setDataPin}) {
  const initialState = {
    isShowing: true,
    loc: '',
    lat: '',
    lng: '',
    desc: '',
    id: 0,
    position: []
  };

  const [state, setState] = useState(initialState);

  useEffect(() => {
    if (getPosition !== null ) {
    setState((prev) => ({
      ...prev,
      lat: getPosition.lat,
      lng: getPosition.lng
    }))
  }    
  }, [getPosition]);

  const handleChange = (e) => {
    const {
      name,
      value
    } = e.target;
    setState(() => ({
      ...state,
      [name]: value
    }));
  };

  const save = (e) => {
      e.preventDefault()
      setState(() => ({
        ...state,
        id: state.id+1,
        loc: "",
        desc: "",
      }));

      let data = dataPin
      let save = {
        id: state.id,
        location: state.loc,
        position: [state.lat, state.lng],
        desc: state.desc
      }
      data.push(save)
      setDataPin(data)
}

  return show ? (
    <div className="toggle toggle-active" >
      <div className="toggle-close">
        <AiFillCloseCircle onClick={() => hide(false)} />
      </div> 
      <div className="preview-picture">
        <img src={Image} />
      </div>
      <div className="preview-description">
        <form className="form-add" onSubmit={save}>
          <label for="loc"> Location Name: </label>
          <input id="loc" type="text" value={state.loc} name="loc" onChange={handleChange} required="required" />
          <label for="lat"> Latitude: </label>
          <input id="lat" type="text" value={state.lat} name="lat" onChange={handleChange} disabled="disabled" />
          <label for="lng"> Longitude: </label>
          <input id="lng" type="text" value={state.lng} name="lng" onChange={handleChange} disabled="disabled" />
          <label for="desc"> Description: </label>
          <input id="desc" type="text" value={state.desc} name="desc" onChange={handleChange} />
          <button className="save" type="submit">Save</button>
        </form>     
      </div>
    </div>
  ) : ""
};

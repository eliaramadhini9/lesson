import { AiFillCloseCircle } from "react-icons/ai";
import React, { useState } from "react";
import Image from './../../../../../image/upload.png';
import "../css/form.css";

export default function EditForm({position}) {
  const initialState = {
    loc: '',
    lat: '',
    lng: '',
    desc: '',
    vehicle: [{
        id: 1,
        vehicle: 'DS1234',
        position: [48.858222, 2.2945],
        timestamp: "2021/05/05 13:01:00"
      },
      {
        id: 2,
        vehicle: 'SD1234',
        position: [48.861111, 2.336389],
        timestamp: "2021/05/05 13:01:00"
      },
      {
        id: 3,
        vehicle: 'TS1234',
        position: [48.853, 2.3498],
        timestamp: "2021/05/05 13:01:00"
      },
      {
        id: 4,
        vehicle: 'DS1234',
        position: [48.86, 2.326389],
        timestamp: "2021/05/05 13:01:00"
      },
    ]
  };

  const [state, setState] = useState(initialState);

  const handleChange = (e) => {
    const {
      name,
      value
    } = e.target;

    setState(() => ({
      ...state,
      [name]: value,
      lat: position.lat,
      lng: position.lng
    }));
  };

  const save = (e) => {
      e.preventDefault()
  }

  return (
    <div className="toggle toggle-active">
      <div className="toggle-close">
        <AiFillCloseCircle />
      </div>
      <div className="preview-picture">
        <img src={Image} />
      </div>
      <div className="preview-description">
        <div className="form-addMark">
          <form className="form-edit" onSubmit={save}>
            <label for="loc"> Location Name: </label>
            <input id="loc" type="text" value={state.loc} name="loc" onChange={handleChange} />
            <label for="lat"> Latitude: </label>
            <input id="lat" type="text" value={state.lat} name="lat" onChange={handleChange} disabled="disabled" />
            <label for="lng"> Longitude: </label>
            <input id="lng" type="text" value={state.lng} name="lng" onChange={handleChange} disabled="disabled" />
            <label for="desc"> Description: </label>
            <input id="desc" type="text" value={state.desc} name="desc" onChange={handleChange} />
            <button type="submit">Save</button>
          </form>
        </div> 
      </div>
    </div>
  );
};

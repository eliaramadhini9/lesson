import React, { useEffect, useState } from "react";
import { IoCaretForwardSharp, IoCaretBackSharp } from "react-icons/io5";
import { FaDollarSign, FaRegCalendarAlt, FaChartBar, FaPowerOff } from "react-icons/fa";
import { BrowserRouter as Router, Link, Route, useLocation } from "react-router-dom";
import Logo from '../image/cadit.jpg';
import '../css/sidebar.css';

function Sidebar() {
  const location = useLocation();
    const initialState = {
        isShowing: true,
    };

    const [state, setState] = useState(initialState);

    const logout = () => {
        localStorage.clear();        
        window.location.reload();
      };


    return (
        <div className="sidebar">
            <div className="sidebar-header">
                <a href='http://localhost:3000'><img className={state.isShowing ? "max" : "min"} src={"http://localhost:3001/cadit.jpg"} alt="logo" /></a>
            </div>
            {state.isShowing ?
                <div className="sidebar-content">
                    <Link className="linkTo" to="/currency">
                        <div className="menu" style={{background: location.pathname === "/currency" ? "rgba(4, 80, 117, .6)" : ""}}>
                            <div className="icon">
                                <FaDollarSign size={30} />
                            </div>
                            <div className="menu-name">Currency</div>
                        </div>
                    </Link>
                    <Link className="linkTo" to="/calendar">
                        <div className="menu" style={{background: location.pathname === "/calendar" ? "rgba(4, 80, 117, .6)" : ""}}>
                            <div className="icon">
                                <FaRegCalendarAlt size={30} />
                            </div>
                            <div className="menu-name">Calendar</div>
                        </div>
                    </Link>
                    <Link className="linkTo" to="/machine_utilization">
                        <div className="menu" style={{background: location.pathname === "/machine_utilization" ? "rgba(4, 80, 117, .6)" : ""}}>
                            <div className="icon">
                                <FaChartBar size={30} />
                            </div>
                            <div className="menu-name">Machine Utilization</div>
                        </div>
                    </Link>
                    <div className="linkTo" onClick={logout}>
                        <div className="menu">
                            <div className="icon">
                                <FaPowerOff size={30} />
                            </div>
                            <div className="menu-name">Logout</div>
                        </div>
                    </div>
                </div>
                :
                <div className="sidebar-content">
                    <Link className="linkTo" to="/currency">
                        <div className="menu" style={{justifyContent: "center", background: location.pathname === "/currency" ? "rgba(4, 80, 117, .6)" : ""}}>
                            <div className="icon">
                                <FaDollarSign size={30} />
                            </div>
<span class="tooltip">Historical Currency</span>
                        </div>
                    </Link>
                    <Link className="linkTo" to="/calendar">
                        <div className="menu" style={{justifyContent: "center", background: location.pathname === "/calendar" ? "rgba(4, 80, 117, .6)" : ""}}>
                            <div className="icon">
                                <FaRegCalendarAlt size={30} />
                            </div>
<span class="tooltip">Calendar</span>
                        </div>
                    </Link>
                    <Link className="linkTo" to="/machine_utilization">
                        <div className="menu" style={{justifyContent: "center", background: location.pathname === "/machine_utilization" ? "rgba(4, 80, 117, .6)" : ""}}>
                            <div className="icon">
                                <FaChartBar size={30} />
                            </div>
<span class="tooltip">Machine Utilization</span>
                        </div>
                    </Link>
                    <div className="linkTo" onClick={logout}>
                        <div className="menu" style={{justifyContent: "center"}}>
                            <div className="icon">
                                <FaPowerOff size={30} />
                            </div>
<span class="tooltip">Logout</span>
                        </div>
                    </div>
                </div>
            }
                {state.isShowing ?
            <div className="sidebar-footer">
                    <IoCaretBackSharp size={30} onClick={() => {
                        setState((prev) => ({
                            ...prev,
                            isShowing: false,
                        }));
                    }} />
            </div>
                    :
            <div className="sidebar-footer" style={{justifyContent: "center"}}>
                    <IoCaretForwardSharp size={30} onClick={() => {
                        setState((prev) => ({
                            ...prev,
                            isShowing: true,
                        }));
                    }} />
            </div>
                }
        </div>
    )
}


export default Sidebar;
